package br.pb.gabrielcbl.page;
import org.openqa.selenium.WebDriver;

import br.pb.gabrielcbl.core.DSL;

public class TestePage {
		
		private DSL dsl;
		
		public TestePage(WebDriver driver) {
			dsl = new DSL(driver);
		}
		public void setNome(String nome) {
			dsl.escrever("username", nome);
		}
		public void setPassword (String nome) {
			dsl.escrever("password", nome);
		}
		public void setComentario (String nome) {
			dsl.escrever("comments", nome);
		}
		public void setCheckBox(int id) {
			dsl.clicarCheckbox("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input["+ id +"]");	
		}
		public void setRadio (int id) {
			dsl.clicarRadio("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input["	+ id +"]");
		}
		public void setSelecaoMultipla(int id) {
			dsl.multipleSelect("multipleselect[]", "Selection Item " + id);
		}
		public void setDesSelecaoMultipla4() {
			dsl.deselect("multipleselect[]", "Selection Item 4");
		}
		public void setDropDown (int id) {
			dsl.selecionarDropDown("dropdown", "/html/body/div/div[3]/form/table/tbody/tr[8]/td/select/option["+ id +"]");
		}
		public void setCancel () {
			dsl.cancel("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[1]");
		}
		public void setSubmtit () {
			dsl.submit("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]");
		}
		public String obterNome() {
			return dsl.obterTextos("_valueusername");
		}
		public String obterPassword () {
			return dsl.obterTextos("_valuepassword");
		}
		public String obterComentario () {
			return dsl.obterTextos("_valuecomments");
		}
		public String obterCheckBox (int id) {
			return dsl.obterTextos("_valuecheckboxes" + id);
		}
		public String obterRadio() {
			return dsl.obterTextos("_valueradioval");
		}
		public String obterSelecaoMultipla(int id) {
			return dsl.obterTextos("_valuemultipleselect" + id);
		}
		public String obterDropDown() {
			return dsl.obterTextos("_valuedropdown");
		}
		public String obterNomeBranco() {
			return dsl.obterTextosBranco("/html/body/div/div[3]/p[1]/strong");
		}
		public String obterPasswordBranco() {
			return dsl.obterTextosBranco("/html/body/div/div[3]/p[2]/strong");
		}
		public String obterComentariosBranco() {
			return dsl.obterTextosBranco("/html/body/div/div[3]/p[3]/strong");
		}
}