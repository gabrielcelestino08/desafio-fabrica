package br.pb.gabrielcbl.core;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DSL {
	
	private WebDriver driver;
	
	public DSL(WebDriver dv) {
		driver = dv;
	}
	/********* Escrever Texto ************/	
	public void escrever(String name_campo, String texto){
		driver.findElement(By.name(name_campo)).clear();
		driver.findElement(By.name(name_campo)).sendKeys(texto);
	}
	
	/********* Clicar Checkbox ************/
	public void clicarCheckbox (String xpath_campo) {
		driver.findElement(By.xpath(xpath_campo)).click();
	}
	/********* Clicar Radio ************/
	public void clicarRadio (String xpath_campo) {
		driver.findElement(By.xpath(xpath_campo)).click();
	}
	/********* Multiple Select Values ************/
	public void multipleSelect(String name_campo, String text) {
		WebElement element = driver.findElement(By.name(name_campo));
		Select combo = new Select(element);
		combo.selectByVisibleText(text);
	}
	public void deselect(String name_campo, String text) {
		WebElement element = driver.findElement(By.name(name_campo));
		Select combo = new Select(element);
		combo.deselectByVisibleText(text);
	}
	/******* DropDown***********/
	public void selecionarDropDown (String name_campo, String xpath) {
		driver.findElement(By.name(name_campo)).click();
		driver.findElement(By.xpath(xpath)).click();
	}
	/**********Submit e Cancel***********/
	public void submit(String xpath_campo) {
		driver.findElement(By.xpath(xpath_campo)).click();
	}
	public void cancel(String xpath_campo) {
		driver.findElement(By.xpath(xpath_campo)).click();
	}
	/******** Obter Textos ***********/
	public String obterTextos(String nome_campo) {
		return driver.findElement(By.id(nome_campo)).getText();
	}
	/********Validar espaços em Branco*********/
	public String obterTextosBranco(String xpath_campo) {
		return driver.findElement(By.xpath(xpath_campo)).getText();
	}
}