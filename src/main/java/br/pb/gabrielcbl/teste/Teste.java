package br.pb.gabrielcbl.teste;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.pb.gabrielcbl.page.TestePage;

public class Teste {

	private WebDriver driver;
	private TestePage page;

	@Before
	public void inicializa() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		String url = "https://testpages.herokuapp.com/styled/basic-html-form-test.html";
		driver.get(url);
		page = new TestePage(driver);
	}

	@After
	public void finaliza() {
		driver.quit();
	}

	@Test
	public void devePreencherCamposCorretamente() {
		page.setNome("Gabriel Celestino");
		page.setPassword("123456");
		page.setComentario("Alguma coisa XD");
		page.setCheckBox(1);
		page.setCheckBox(2);
		page.setRadio(1);
		page.setDesSelecaoMultipla4();
		page.setSelecaoMultipla(1);
		page.setSelecaoMultipla(3);
		page.setDropDown(5);
		page.setSubmtit();

		Assert.assertEquals("Gabriel Celestino", page.obterNome());
		Assert.assertEquals("123456", page.obterPassword());
		Assert.assertEquals("Alguma coisa XD", page.obterComentario());
		Assert.assertEquals("cb1", page.obterCheckBox(0));
		Assert.assertEquals("cb2", page.obterCheckBox(1));
		Assert.assertEquals("cb3", page.obterCheckBox(2));
		Assert.assertEquals("rd1", page.obterRadio());
		Assert.assertEquals("ms1", page.obterSelecaoMultipla(0));
		Assert.assertEquals("ms3", page.obterSelecaoMultipla(1));
		Assert.assertEquals("dd5", page.obterDropDown());
	}

	@Test
	public void deveValidarCamposEmBranco() {
		page.setNome("");
		page.setPassword("");
		page.setComentario("");
		page.setCheckBox(2);
		page.setRadio(3);
		page.setSelecaoMultipla(1);
		page.setSelecaoMultipla(2);
		page.setDropDown(2);
		page.setSubmtit();

		Assert.assertEquals("No Value for username", page.obterNomeBranco());
		Assert.assertEquals("No Value for password", page.obterPasswordBranco());
		Assert.assertEquals("No Value for comments", page.obterComentariosBranco());
		Assert.assertEquals("cb2", page.obterCheckBox(0));
		Assert.assertEquals("cb3", page.obterCheckBox(1));
		Assert.assertEquals("rd3", page.obterRadio());
		Assert.assertEquals("ms1", page.obterSelecaoMultipla(0));
		Assert.assertEquals("ms2", page.obterSelecaoMultipla(1));
		Assert.assertEquals("ms4", page.obterSelecaoMultipla(2));
		Assert.assertEquals("dd2", page.obterDropDown());

	}
}	
